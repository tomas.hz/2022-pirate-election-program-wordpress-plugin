<?php
	function pirate_program_api() {
		global $wpdb;
		$table_name = $wpdb->prefix . "pirate_program_entries";
		
		if ($_SERVER["REQUEST_METHOD"] === "GET") {
			// List entries
			
			$results = $wpdb->get_results(
				"SELECT point, COUNT(point) AS amount
				FROM $table_name
				GROUP BY point"
			);
			
			$json_results = Array();
			
			if ($wpdb->num_rows !== 0) {
				foreach ($results as $row) {
					$json_results[$row->point] = intval($row->amount);
				}
			}
			
			return $json_results;
		} elseif ($_SERVER["REQUEST_METHOD"] === "POST") {
			// Add entry
			
			// It's hard to obscure the remote IP well with PHP and Wordpress, but
			// we can at least delete old entries.
			
			// Max 7 days ago
			$current_time = time();
			$max_timestamp = $current_time - 604800;
			$wpdb->query(
				"UPDATE $table_name
				SET identifier = NULL
				WHERE timestamp < $max_timestamp"
			);
			
			$ip = $_SERVER["REMOTE_ADDR"];
			$point = json_decode(file_get_contents('php://input'))->point;
			
			if (
				$wpdb->get_row(
					"SELECT id
					FROM $table_name
					WHERE
						identifier = \"$ip\" AND
						point = \"$point\""
				) !== null
			) {
				return new WP_Error(
					"pirate_program_vote_exists",
					"Vote already exists for this point",
					Array("status" => 403)
				);
			}
			
			$id = uniqid();
			
			$wpdb->query(
				"INSERT INTO $table_name (id, timestamp, identifier, point)
				VALUES (\"$id\", $current_time, \"$ip\", \"$point\");"
			);
			
			return null;
		} elseif ($_SERVER["REQUEST_METHOD"] === "DELETE") {
			// Delete entry
			
			$ip = $_SERVER["REMOTE_ADDR"];
			$point = json_decode(file_get_contents('php://input'))->point;
			
			if (
				$wpdb->get_row(
					"SELECT id
					FROM $table_name
					WHERE
						identifier = \"$ip\" AND
						point = \"$point\""
				) === null
			) {
				return new WP_Error(
					"pirate_program_point_or_vote_inexistent",
					"Point or vote does not exist",
					Array("status" => 403)
				);
			}
			
			$wpdb->query(
				"DELETE FROM $table_name
				WHERE
					identifier = \"$ip\" AND
					point = \"$point\""
			);
			
			return null;
		}
	}
?>
