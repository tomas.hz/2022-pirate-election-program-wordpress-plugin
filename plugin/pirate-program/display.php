<?php
	get_header();
	
	wp_enqueue_script("jquery");
	
	$options = get_option("pirate_program_plugin_options");
	$content = $options["content"];
	
	global $wpdb;
	$table_name = $wpdb->prefix . "pirate_program_entries";
	
	$votes = $wpdb->get_results(
		"SELECT point, COUNT(point) AS amount
		FROM $table_name
		GROUP BY point"
	);
	
	$grouped_votes = Array();
	$point_styles = "";
	
	if ($wpdb->num_rows !== 0) {
		foreach ($votes as $row) {
			$grouped_votes[$row->point] = $row->amount;
		}
	}
	
	$upload_dirs = wp_upload_dir();
	
	$upload_location_url = $upload_dirs["baseurl"];
	$upload_location_fs = $upload_dirs["basedir"];
?>

	<script>
		const MODAL_JS_URL = "<?php echo home_url("/wp-content/plugins/pirate-program/js/jquery.modal.js") ?>";
	</script>
	<script
		src="<?php echo home_url("/wp-content/plugins/pirate-program/js/display.js") ?>"
	></script>
	<script
		src="<?php echo home_url("/wp-content/plugins/pirate-program/js/mast-share.js") ?>"
	></script>
	<script
		src="<?php echo home_url("/wp-content/plugins/pirate-program/js/js.cookie.min.js") ?>"
	></script>
	
	<script>
		const API_URL = "<?php echo get_rest_url(null, "pirate-program/votes") ?>";
	</script>
	
	<link
		rel="stylesheet"
		href="<?php echo home_url("/wp-content/plugins/pirate-program/css/display.css") ?>"
	>
	<link
		rel="stylesheet"
		href="<?php echo home_url("/wp-content/plugins/pirate-program/css/jquery.modal.css") ?>"
	>
	<link
		rel="stylesheet"
		href="<?php echo home_url("/wp-content/plugins/pirate-program/css/mast-share.css") ?>"
	>
	<link
		rel="stylesheet"
		href="<?php echo home_url("/wp-content/plugins/pirate-program/fonts/bebas-neue/style.css") ?>"
	>
	<link
		rel="stylesheet"
		href="<?php echo home_url("/wp-content/plugins/pirate-program/fonts/open-sans/style.css") ?>"
	>
	
	<?php if ($options["heading"] !== "") { ?>
		<h1 class="program-heading"><?php echo $options["heading"] ?></h1>
	<?php } ?>
	
	<section class="program-main-wrapper">
		<div class="program-content-wrapper">
			<section class="program-topic-wrapper">
				<?php
					foreach($content as $topic) {
				?>
					<section class="program-topic" id="<?php echo $topic->id ?>">
						<label><?php echo $topic->name ?></label>
						
						<svg>
							<use xlink:href="<?php
								$topic_icon = $topic->icon;
								echo home_url("/wp-content/plugins/pirate-program/images/theme-icons/$topic_icon.svg#icon")
							?>"></use>
						</svg>
					</section>
				<?php
					}
				?>
			</section>
			
			<section class="program-topic-content-wrapper" style="max-height:<?php echo ceil(count($content) / 3) * 115.75 ?>px">
				<?php
					foreach($content as $topic) {
				?>
					<div
						id="<?php echo $topic->id ?>-content"
						class="program-topic-content"
						style="display:none"
					>
						<h2 style="color:#000">
							<?php
								echo $topic->heading;
								
								if ($topic->has_audio) {
							?>
							
								<a
									class="audio-link"
									href="<?php
										$filename = "pirate-program-point-audio-" . $topic->id;
										$joined_url_path = path_join($upload_location_url, $filename);
										
										echo $joined_url_path;
									?>"
									type="<?php
										$joined_path = path_join($upload_location_fs, $filename);
										
										$file = fopen($joined_path, "r");
										
										$file_info = new finfo(FILEINFO_MIME);
										
										$file_buffer = fread($file, filesize($joined_path));
										$media_type = explode(";", $file_info->buffer($file_buffer))[0];
										
										fclose($file);
										
										echo $media_type;
									?>"
									target="_blank"
								><img
									src="/wp-content/plugins/pirate-program/images/headphones.svg"
									width="20"
									height="20"
								></a>
							
							<?php } ?>
						</h2>
						
						<?php echo $topic->description ?>
						
						<?php
							foreach($topic->points as $point) {
						?>
							<div class="program-point" id="<?php echo $point->id ?>">
								<div class="program-point-text">
									<?php echo $point->text ?>
								</div>
								
								<div class="program-point-button-wrapper">
									<svg class="program-point-like" title="Líbí se mi">
										<use xlink:href="<?php echo home_url("/wp-content/plugins/pirate-program/images/heart.svg#icon") ?>"></use>
									</svg>
									<svg class="program-point-share" title="Sdílet">
										<use xlink:href="<?php echo home_url("/wp-content/plugins/pirate-program/images/share.svg#icon") ?>"></use>
									</svg>
								</div>
							</div>
						<?php
							$id = $point->id;
							$vote_count = $grouped_votes[$point->id];
							
							$point_styles = $point_styles . "[id=\"$id\"] .program-point-button-wrapper::after {
								content: \"$vote_count\";
							}
							";
							
							}
						?>
					</div>
				<?php
					}
				?>
			</section>
		</div>
		
		<?php if ($options["has_audio"]) { ?>
		
			<a
				class="alternate-program-version"
				href="<?php
					$filename = "pirate-program-full-audio";
					$joined_url_path = path_join($upload_location_url, $filename);
					
					echo $joined_url_path;
				?>"
				type="<?php
					$joined_path = path_join($upload_location_fs, $filename);
					
					$file = fopen($joined_path, "r");
					
					$file_info = new finfo(FILEINFO_MIME);
					
					$file_buffer = fread($file, filesize($joined_path));
					$media_type = explode(";", $file_info->buffer($file_buffer))[0];
					
					fclose($file);
					
					echo $media_type;
				?>"
			>Audio verze celého programu</a> | 
		
		<?php } ?>
		
		<?php if ($options["has_document"]) { ?>
		
			<a
				class="alternate-program-version"
				href="<?php
					$filename = "pirate-program-document-version";
					$joined_url_path = path_join($upload_location_url, $filename);
					
					echo $joined_url_path;
				?>"
				type="<?php
					$joined_path = path_join($upload_location_fs, $filename);
					
					$file = fopen($joined_path, "r");
					
					$file_info = new finfo(FILEINFO_MIME);
					
					$file_buffer = fread($file, filesize($joined_path));
					$media_type = explode(";", $file_info->buffer($file_buffer))[0];
					
					fclose($file);
					
					echo $media_type;
				?>"
			>Verze v dokumentu</a>
		
		<?php } ?>
	</section>
	
	<section class="bottom-buttons-wrapper">
		<?php if ($options["link_newsletter"] !== "" and $options["link_newsletter"] !== null) { ?>
			<a
				class="bottom-button"
				id="button-odebirej-novinky"
				href="<?php echo $options["link_newsletter"] ?>"
				target="_blank"
			>Odebírej<br>Novinky</a>
		<?php } ?>
		
		<?php if ($options["link_onboarding"] !== "" and $options["link_onboarding"] !== null) { ?>
			<a
				class="bottom-button"
				id="button-pridej-se-k-nam"
				href="<?php echo $options["link_onboarding"] ?>"
				target="_blank"
			>Přidej se<br>k nám!</a>
		<?php } ?>
		
		<?php if ($options["link_donations"] !== "" and $options["link_donations"] !== null) { ?>
			<a
				class="bottom-button"
				id="button-prihod-do-truhly"
				href="<?php echo $options["link_donations"] ?>"
				target="_blank"
			>Přihoď<br>do truhly</a>
		<?php } ?>
	</section>
	
	<style>
		<?php echo $point_styles ?>
	</style>
	
	<section id="share-form" class="modal">
		<h2>Sdílení je aktem lásky.</h2>
		
		<textarea id="share-text" rows="5"></textarea>
		
		
		<section class="share-icon-wrapper">
			<div class="mast-share mast-share-sm share-icon">
				<input type="checkbox" class="mast-check-toggle">
				<div class="mast-instance">
					<span>Instance: </span>
					<input type="textbox" name="mast-instance-input" placeholder="mastodon.social">
					<button class="mast-share-button">Sdílet</button>
				</div>
				<label class="mast-top mast-check-label">
					<img
						src="<?php echo home_url("/wp-content/plugins/pirate-program/images/share-icons/mastodon-brands.svg") ?>"
						alt="Sdílet na Mastodonu"
						height="40"
						width="40"
					>
				</label>
			</div>
			
			<a
				id="share-diaspora"
				class="share-icon"
				href="#"
				rel="nofollow noopener noreferrer"
			><img
				src="<?php echo home_url("/wp-content/plugins/pirate-program/images/share-icons/diaspora-brands.svg") ?>"
				alt="Sdílet na Diaspoře"
				height="40"
				width="40"
			></a>
			
			<a
				id="share-twitter"
				class="share-icon"
				href="#"
				rel="nofollow noopener noreferrer"
			><img
				src="<?php echo home_url("/wp-content/plugins/pirate-program/images/share-icons/twitter-square-brands.svg") ?>"
				alt="Sdílet na Twitteru"
				height="40"
				width="40"
			></a>
			
			<a
				id="share-facebook"
				class="share-icon"
				href="#"
				rel="nofollow noopener noreferrer"
			><img
				src="<?php echo home_url("/wp-content/plugins/pirate-program/images/share-icons/facebook-square-brands.svg") ?>"
				alt="Sdílet na Facebooku"
				height="40"
				width="40"
			></a>
		</section>
	</section>

<?php
	get_footer();
?>
