var program = {
	topics: [],
	audio_version: null,
	document_version: null
};

// https://stackoverflow.com/a/36281449
// Thanks to Dmitri Pavlutin!
async function getBase64(file) {
	let result = null;
	
	const readerPromise = new Promise(
		resolve => {
			const reader = new FileReader();
			reader.readAsDataURL(file);
			
			reader.onload = function () {
				result = reader.result;
				resolve();
			};
			
			reader.onerror = function (error) {
				console.log("Reader error: ", error);
			};
		}
	);
	
	await readerPromise;
	
	return result;
}

// https://learnbatta.com/blog/how-to-add-image-in-select-options-html-93/
// https://stackoverflow.com/a/32866520
// Thanks to Jay Rizzi and learnBATTA!
function formatSelect2ProgramIconData(data){
	if (data.disabled || data.loading) {
		return data.text;
	}
	
	return jQuery(`
		<span class="select2-image-wrapper">
			<img
				src="../wp-content/plugins/pirate-program/images/theme-icons/${data.element.value}.svg"
				class="select2-image"
				alt="${data.text}"
			>
			<span class="select2-text">${data.text}</span>
		</span>
	`);
}

jQuery(document).ready(
	function() {
		jQuery("#topic-add").on(
			"click",
			function() {
				program.topics.push(
					{
						id: "",
						icon: "",
						name: "",
						heading: "",
						description: "",
						points: [],
						audio: null
					}
				);
				
				const topicNumber = program.topics.length - 1;
				
				jQuery("#topic-wrapper").append(
					jQuery(`
						<section id="topic-${topicNumber}" class="topic">
							<label for="topic-${topicNumber}-icon">Ikona <span class="input-required">*</span></label>
							
							<select
								id="topic-${topicNumber}-icon"
								name="topic-${topicNumber}-icon"
								autocomplete="off"
							>
								<option
									value="verejne-sluzby"
								>Veřejné služby</option>
								<option
									value="zdravotni-pece"
								>Zdravotní péče</option>
								<option
									value="sport"
								>Sport</option>
								<option
									value="socialni-politika"
								>Sociální politika</option>
								<option
									value="aktivni-verejnost"
								>Aktivní veřejnost</option>
								<option
									value="priroda"
								>Příroda</option>
								<option
									value="vzdelavani"
								>Vzdělávání</option>
								<option
									value="kultura"
								>Kultura</option>
								<option
									value="otevrena-radnice"
								>Otevřená radnice</option>
								<option
									value="cestovni-ruch"
								>Cestovní ruch</option>
								<option
									value="doprava"
								>Doprava</option>
								<option
									value="bydleni"
								>Bydlení</option>
							</select>
							
							\<script\>
								jQuery("#topic-${topicNumber}-icon").select2({
									templateSelection: formatSelect2ProgramIconData,
									templateResult: formatSelect2ProgramIconData,
									selectionCssClass: "select2-container-small-images",
									dropdownCssClass: "select2-container-small-images",
									width: "100%"
								});
								
								jQuery("#topic-${topicNumber}-icon").on(
									"select2:select",
									function(event) {
										const value = event.params.data.element.value;
										
										program.topics[${topicNumber}].icon = value;
									}
								);
								
								program.topics[${topicNumber}].icon = (
									jQuery("#topic-${topicNumber}-icon")
									.select2("data")[0]
									.element.value
								);
							\</script\>
							
							<label for="topic-${topicNumber}-name">Název <span class="input-required">*</span></label>
							
							<input
								type="text"
								id="topic-${topicNumber}-name"
								name="topic-${topicNumber}-name"
								class="pirate-input"
								autocomplete="off"
							>
							
							\<script\>
							jQuery("#topic-${topicNumber}-name").on(
									"input",
									function(event) {
										program.topics[${topicNumber}].name = event.target.value;
									}
								);
							\</script\>
							
							<label for="topic-${topicNumber}-heading">Nadpis <span class="input-required">*</span></label>
							
							<input
								type="text"
								id="topic-${topicNumber}-heading"
								name="topic-${topicNumber}-heading"
								class="pirate-input"
								autocomplete="off"
							>
							
							\<script\>
								jQuery("#topic-${topicNumber}-heading").on(
									"input",
									function(event) {
										program.topics[${topicNumber}].heading = event.target.value;
									}
								);
							\</script\>
							
							<div class="topic-audio-version-wrapper">
								<label
									for="topic-${topicNumber}-audio"
								>Audio verze</label>
								<input
									type="file"
									id="topic-${topicNumber}-audio"
									name="topic-${topicNumber}-audio"
									accept="audio/*"
								>
							</div>
								
							\<script type="text/javascript"\>
								jQuery(document).ready(
									function() {
										jQuery("#topic-${topicNumber}-audio").on(
											"input",
											async function(event) {
												program.topics[${topicNumber}].audio = await getBase64(event.target.files[0]);
											}
										);
									}
								);
							\</script\>
							
							<label for="topic-${topicNumber}-description">Popis témata</label>
							
							<textarea
								id="topic-${topicNumber}-description"
								name="topic-${topicNumber}-description"
								rows="5"
								autocomplete="off"
							></textarea>
							
							\<script\>
								wp.editor.initialize(
									"topic-${topicNumber}-description",
									{
										tinymce: {
											init_instance_callback: function(editor) {
												editor.on(
													"change",
													function(event) {
														event.stopPropagation();
														program.topics[${topicNumber}].description = editor.getContent();
													}
												);
												
												editor.on(
													"input",
													function(input) {
														program.topics[${topicNumber}].description = editor.getContent();
													}
												);
											}
										}
									}
								);
							\</script\>
							
							<hr>
							
							<h2>Body témata</h2>
							
							<section class="point-wrapper">
								<section id="topic-${topicNumber}-points">
								
								</section>
								
								<button
									class="add-button"
									id="topic-${topicNumber}-add-point"
								>Přidat bod</button>
							</section>
							
							<button
								class="delete-button"
								id="topic-${topicNumber}-delete"
							>Smazat téma</button>
							
							<script>
								jQuery("#topic-${topicNumber}-delete").on(
									"click",
									function() {
										jQuery("#topic-${topicNumber}").remove();
										
										program.topics.splice(${topicNumber}, 1);
										
										jQuery("#save").click();
									}
								);
							</script>
						</section>
					`)
				);
				
				jQuery(`#topic-${topicNumber}-add-point`).on(
					"click",
					function() {
						program.topics[topicNumber].points.push({text: "", id: ""});
						
						const pointNumber = program.topics[topicNumber].points.length - 1;
						
						jQuery(`#topic-${topicNumber}-points`).append(
							jQuery(`
								<section id="topic-${topicNumber}-point-${pointNumber}" class="point">
									<textarea
										id="topic-${topicNumber}-point-${pointNumber}-content"
										name="topic-${topicNumber}-point-${pointNumber}-content"
										rows="5"
										autocomplete="off"
									></textarea>
									
									<button
										class="delete-button"
										id="topic-${topicNumber}-point-${pointNumber}-delete"
									>Smazat bod</button>
									
									\<script\>
										jQuery("#topic-${topicNumber}-point-${pointNumber}-delete").on(
											"click",
											function() {
												jQuery("#topic-${topicNumber}-point-${pointNumber}").remove();
												
												(
													program.topics[${topicNumber}]
													.points
													.splice(${pointNumber}, 1)
												);
												
												jQuery("#save").click();
											}
										);
										
										wp.editor.initialize(
											"topic-${topicNumber}-point-${pointNumber}-content",
											{
												tinymce: {
													init_instance_callback: function(editor) {
														
														// Work around TinyMCE's pasting bug(?)
														editor.on(
															"change",
															function(event) {
																event.stopPropagation();
																
																program.topics[${topicNumber}]
																.points[${pointNumber}]
																.text = editor.getContent();
															}
														);
														
														editor.on(
															"input",
															function(event) {
																program.topics[${topicNumber}]
																.points[${pointNumber}]
																.text = editor.getContent();
															}
														);
													}
												}
											}
										);
									\</script\>
								</section>
							`)
						);
					}
				);
			}
		);
		
		jQuery("#save").on(
			"click",
			async function() {
				jQuery(this).prop("disabled", true); // Don't make an accidental double click ruin everything
				
				let usedIds = [];
				
				for (const topic of program.topics) {
					if (topic.id === "") {
						let id = null;
						
						do {
							id = uuidv4();
							topic.id = id;
						} while (usedIds.includes(id));
						
						usedIds.push(id);
					} else {
						usedIds.push(topic.id);
					}
					
					for (const point of topic.points) {
						// Just in case
						if (point.id === "") {
							let id = null;
							
							do {
								id = uuidv4();
								point.id = id;
							} while (usedIds.includes(id));
							
							usedIds.push(id);
						} else {
							usedIds.push(point.id);
						}
					}
				}
				
				let formData = new FormData(document.getElementById("xss-form"));
				formData.append(
					"pirate_program_plugin_options[content]",
					JSON.stringify(program.topics)
				);
				formData.append(
					"pirate_program_plugin_options[heading]",
					jQuery("#pirate_program_plugin_options\\[heading\\]").val()
				);
				
				if (program.audio_version !== null) {
					formData.append(
						"pirate_program_plugin_options[audio_version]",
						program.audio_version
					);
				} else {
					formData.append(
						"pirate_program_plugin_options[audio_version]",
						""
					);
				}
				
				if (program.document_version !== null) {
					formData.append(
						"pirate_program_plugin_options[document_version]",
						program.document_version
					);
				} else {
					formData.append(
						"pirate_program_plugin_options[document_version]",
						""
					);
				}
				
				formData.append(
					"pirate_program_plugin_options[link_newsletter]",
					jQuery("#pirate_program_plugin_options\\[link_newsletter\\]").val()
				);
				formData.append(
					"pirate_program_plugin_options[link_onboarding]",
					jQuery("#pirate_program_plugin_options\\[link_onboarding\\]").val()
				);
				formData.append(
					"pirate_program_plugin_options[link_donations]",
					jQuery("#pirate_program_plugin_options\\[link_donations\\]").val()
				);
				
				formData.append(
					"pirate_program_plugin_options[post_id]",
					jQuery("#pirate_program_plugin_options\\[post_id\\]").val()
				);
				
				fetch(
					"options.php",
					{
						method: "POST",
						body: formData
					}
				).then(
					function(result) {
						if (!result.ok) {
							alert("Chyba při uložení. Jsou vložená data správně?");
							console.log("Error saving data: ", result);
						}
						
						window.location.reload();
					}
				);
			}
		);
	}
);
