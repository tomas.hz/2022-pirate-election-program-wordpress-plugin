var votedPoints = [];

document.addEventListener(
	"DOMContentLoaded",
	function(event) {
		const rawVotedPoints = Cookies.get("voted-points");
		
		if (rawVotedPoints !== undefined) {
			votedPoints = JSON.parse(rawVotedPoints);
		}
		
		const modalScript = document.createElement("script");
		modalScript.src = MODAL_JS_URL;
		document.head.append(modalScript);
		
		jQuery(".program-topic").on(
			"click",
			function(event) {
				jQuery(".program-topic").removeClass("program-topic-selected");
				jQuery(".program-topic-content").css("display", "none");
				
				const element = jQuery(event.currentTarget);
				
				if (element.hasClass("program-topic-selected")) {
					element.removeClass("program-topic-selected");
				} else {
					element.addClass("program-topic-selected");
					
					jQuery(`#${element[0].id}-content`).css("display", "block");
				}
			}
		);
		
		jQuery(".program-point-like").on(
			"click",
			function(event) {
				const element = event.currentTarget;
				const pointName = element.parentNode.parentNode.id;
				
				if (jQuery(element).hasClass("program-point-like-used")) {
					jQuery(element).removeClass("program-point-like-used");
					
					fetch(
						API_URL,
						{
							method: "DELETE",
							headers: {
								"Content-Type": "application/json"
							},
							body: JSON.stringify({
								point: pointName
							})
						}
					).then(
						function(response) {
							if (!response.ok) {
								jQuery(element).addClass("program-point-like-used");
								
								alert("Zrušení hlasu se nezdařilo. Zkus to prosím znovu za krátkou chvíli, nebo nás kontaktuj.");
								return;
							}
							
							
							// https://stackoverflow.com/a/20827100
							// Thanks to TuralAsgar and C B!
							votedPoints = votedPoints.filter(e => e !== pointName);
							Cookies.set(
								"voted-points",
								JSON.stringify(votedPoints),
								{sameSite: "strict"}
							);
						}
					);
					
					return;
				}
				
				jQuery(element).addClass("program-point-like-used");
				
				fetch(
					API_URL,
					{
						method: "POST",
						headers: {
							"Content-Type": "application/json"
						},
						body: JSON.stringify({
							point: pointName
						})
					}
				).then(
					function(response) {
						if (!response.ok) {
							jQuery(element).removeClass("program-point-like-used");
							
							alert("Hlasování se nezdařilo. Zkus to prosím znovu za krátkou chvíli, nebo nás kontaktuj.");
							return;
						}
						
						votedPoints.push(pointName);
						Cookies.set(
							"voted-points",
							JSON.stringify(votedPoints),
							{sameSite: "strict"}
						);
					}
				);
			}
		);
		
		jQuery(".program-point-share").on(
			"click",
			function(event) {
				jQuery("#share-form").modal();
				
				const topicParent = event.currentTarget.parentNode.parentNode;
				const url = `https://pirati.cz/program/komunal2022/body/${topicParent.id}.html`;
				
				const shareTextElement = jQuery("#share-text");
				const renderedSharedText = (
					"„"
					// Remove padding tabs and newlines, replace tabs with newlines and only allow one consecutive line break
					+ topicParent.textContent.trim().replace(/\t+/g, "\n").replace(/\n\s*\n/g, "\n")
					+ "“\n\n"
					+ "https://pirati.cz/program/komunal2022/"
				);
				
				shareTextElement.html(renderedSharedText);
				shareTextElement.select();
				
				jQuery("#share-facebook").attr(
					"href",
					"https://facebook.com/sharer/sharer.php?u="
					+ encodeURIComponent(url)
				);
				jQuery("#share-twitter").attr(
					"href",
					`https://twitter.com/share?text=${encodeURIComponent(renderedSharedText)}`
				);
				jQuery("#share-diaspora")[0].onclick = function(event) {
					window.open(
						(
							"https://sharetodiaspora.github.io/?url="
							+ encodeURIComponent(url)
							+ "&title="
							+ encodeURIComponent(document.title)
						),
						"das",
						"location=no,links=no,scrollbars=no,toolbar=no,width=620,height=550"
					);
				}
			}
		);
		
		for (const point of votedPoints) {
			jQuery(`#${point}`).
			children("div.program-point-button-wrapper").
			children("svg.program-point-like").
			addClass("program-point-like-used");
		}
		
		jQuery(".program-topic")[0].click();
	}
);
