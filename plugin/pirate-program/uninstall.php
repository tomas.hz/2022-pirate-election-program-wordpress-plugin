<?php
	// if uninstall.php is not called by WordPress, die
	if (!defined("WP_UNINSTALL_PLUGIN")) {
		die;
	}
	
	global $wpdb;
	
	$option_name = "pirate_program_plugin_options";
	
	$post_id = get_options($option_name);
	wp_delete_post($post_id, true);
	
	delete_option("pirate_program_plugin_options");
	delete_site_option("pirate_program_plugin_options");
	
	$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}pirate_program_entries");
?>
