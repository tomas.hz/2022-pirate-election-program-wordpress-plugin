<?php
/*
Plugin Name:  Stránka pro pirátksý program
Plugin URI:   https://toho.neocities.org
Description:  Plugin, který přidává interaktivní program pro komunální volby 2022.
Version:      0.2
Author:       Tomáš Valenta
Author URI:   https://toho.neocities.org
License:      AGPLv3
License URI:  https://www.gnu.org/licenses/agpl-3.0.en.html
Text Domain:  pirate_party_program
Domain Path:  /languages
*/

// Page rendering
function pirate_program_render_plugin_settings_page() {
	?>
		<script src="../wp-content/plugins/pirate-program/js/select2.js"></script>
		<script src="../wp-content/plugins/pirate-program/js/admin.js"></script>
		<script src="../wp-content/plugins/pirate-program/js/uuidv4.min.js"></script>
		
		<link rel="stylesheet" href="../wp-content/plugins/pirate-program/css/select2.css">
		<link rel="stylesheet" href="../wp-content/plugins/pirate-program/css/pirate-program-settings.css">
		
		<h2>Nastavení pirátského programu</h2>
		
		<form id="xss-form">
			<?php 
				settings_fields("pirate_program_plugin_options");
			?>
		</form>
		
		<?php
			do_settings_sections("pirate_party_program");
		?>
		
		<button
			class="save-button"
			id="save"
		>Uložit</button>
	<?php
}

function pirate_program_section_text() {
	// Nothing necessary, but keep the function
}

function pirate_program_dev() {
	// Nothing necessary, but keep the function
}

// Functions for inputs
function pirate_program_plugin_options_validate($input) {
	// We don't *really* care about input validation here, since this will
	// only be exposed to admins. It'd be good to have it, but it's not a
	// current priority with the relatively short amount of time left for
	// everything.
	
	$decoded_input = json_decode($input["content"]);
	
	$upload_location = wp_upload_dir()["basedir"];
	
	if ($decoded_input === null) {
		$decoded_input = Array();
	} else {
		foreach ($decoded_input as $key => $value) {
			$filename = "pirate-program-point-audio-" . $value->id;
			$joined_path = path_join($upload_location, $filename);
			$decoded_input[$key]->has_audio = false;
			
			if ($value->audio !== null) {
				$file = fopen($joined_path, "w");
				
				$base64_content = explode(";base64,", $value->audio)[1];
				
				fwrite(
					$file,
					base64_decode($base64_content)
				);
				
				fclose($file);
				
				$decoded_input[$key]->has_audio = true;
			} elseif (file_exists($joined_path)) {
				unlink($joined_path);
			}
			
			unset($decoded_input[$key]->audio);
		}
	}
	
	$has_audio_version = false;
	$joined_audio_path = path_join($upload_location, "pirate-program-full-audio");
	
	if ($input["audio_version"] !== "") {
		$file = fopen($joined_audio_path, "w");
		
		$base64_content = explode(";base64,", $input["audio_version"])[1];
		
		fwrite(
			$file,
			base64_decode($base64_content)
		);
		
		fclose($file);
		
		$has_audio_version = true;
	} elseif (file_exists($joined_audio_path)) {
		unlink($joined_audio_path);
	}
	
	unset($input["audio_version"]);
	
	$has_document_version = false;
	$joined_document_path = path_join($upload_location, "pirate-program-document-version");
	
	if ($input["document_version"] !== "") {
		$file = fopen($joined_document_path, "w");
		
		$base64_content = explode(";base64,", $input["document_version"])[1];
		
		fwrite(
			$file,
			base64_decode($base64_content)
		);
		
		fclose($file);
		
		$has_document_version = true;
	} elseif (file_exists($joined_document_path)) {
		unlink($joined_document_path);
	}
	
	unset($input["document_version"]);
	
	return Array(
		"post_id" => $input["post_id"],
		"heading" => $input["heading"],
		"content" => $decoded_input,
		"has_audio" => $has_audio_version,
		"has_document" => $has_document_version,
		"link_newsletter" => $input["link_newsletter"],
		"link_onboarding" => $input["link_onboarding"],
		"link_donations" => $input["link_donations"],
	);
}

function pirate_party_program_content() {
	$content = get_option("pirate_program_plugin_options")["content"];
	
	?>
		<script>
			var program = {
				topics: <?php
					if ($content !== null) {
						echo json_encode($content);
					} else {
						echo "[]";
					}
				?>,
				audio_version: null,
				document_version: null
			};
		</script>
		
		<section id="topic-wrapper">
			<?php
				$topic_position = 0;
				$upload_location = wp_upload_dir()["basedir"];
				
				foreach($content as $key => $value) {
					?>
					
						<section id="topic-<?php echo $topic_position ?>" class="topic">
							<label for="topic-<?php echo $topic_position ?>-icon">Ikona <span class="input-required">*</span></label>
							
							<select
								id="topic-<?php echo $topic_position ?>-icon"
								name="topic-<?php echo $topic_position ?>-icon"
								autocomplete="off"
							>
								<option
									value="verejne-sluzby"
									<?php if ($value->icon === "verejne-sluzby") { echo "selected"; } ?>
								>Veřejné služby</option>
								<option
									value="zdravotni-pece"
									<?php if ($value->icon === "zdravotni-pece") { echo "selected"; } ?>
								>Zdravotní péče</option>
								<option
									value="sport"
									<?php if ($value->icon === "sport") { echo "selected"; } ?>
								>Sport</option>
								<option
									value="socialni-politika"
									<?php if ($value->icon === "socialni-politika") { echo "selected"; } ?>
								>Sociální politika</option>
								<option
									value="aktivni-verejnost"
									<?php if ($value->icon === "aktivni-verejnost") { echo "selected"; } ?>
								>Aktivní veřejnost</option>
								<option
									value="priroda"
									<?php if ($value->icon === "priroda") { echo "selected"; } ?>
								>Příroda</option>
								<option
									value="vzdelavani"
									<?php if ($value->icon === "vzdelavani") { echo "selected"; } ?>
								>Vzdělávání</option>
								<option
									value="kultura"
									<?php if ($value->icon === "kultura") { echo "selected"; } ?>
								>Kultura</option>
								<option
									value="otevrena-radnice"
									<?php if ($value->icon === "otevrena-radnice") { echo "selected"; } ?>
								>Otevřená radnice</option>
								<option
									value="cestovni-ruch"
									<?php if ($value->icon === "cestovni-ruch") { echo "selected"; } ?>
								>Cestovní ruch</option>
								<option
									value="doprava"
									<?php if ($value->icon === "doprava") { echo "selected"; } ?>
								>Doprava</option>
								<option
									value="bydleni"
									<?php if ($value->icon === "bydleni") { echo "selected"; } ?>
								>Bydlení</option>
							</select>
							
							<script>
								jQuery(document).ready(
									function() {
										jQuery("#topic-<?php echo $topic_position ?>-icon").select2({
											templateSelection: formatSelect2ProgramIconData,
											templateResult: formatSelect2ProgramIconData,
											selectionCssClass: "select2-container-small-images",
											dropdownCssClass: "select2-container-small-images",
											width: "100%"
										});
										
										jQuery("#topic-<?php echo $topic_position ?>-icon").on(
											"select2:select",
											function(event) {
												const value = event.params.data.element.value;
												
												program.topics[<?php echo $topic_position ?>].icon = value;
											}
										);
									}
								);
							</script>
							
							<label for="topic-<?php echo $topic_position ?>-name">Název <span class="input-required">*</span></label>
							
							<input
								type="text"
								id="topic-<?php echo $topic_position ?>-name"
								name="topic-<?php echo $topic_position ?>-name"
								class="pirate-input"
								value="<?php echo $value->name ?>"
								autocomplete="off"
								required
							>
							
							<script>
								jQuery(document).ready(
									function() {
										jQuery("#topic-<?php echo $topic_position ?>-name").on(
											"input",
											function(event) {
												program.topics[<?php echo $topic_position ?>].name = event.target.value;
											}
										);
									}
								);
							</script>
							
							<label for="topic-<?php echo $topic_position ?>-heading">Nadpis <span class="input-required">*</span></label>
							
							<input
								type="text"
								id="topic-<?php echo $topic_position ?>-heading"
								name="topic-<?php echo $topic_position ?>-heading"
								class="pirate-input"
								value="<?php echo $value->heading ?>"
								autocomplete="off"
								required
							>
							
							<script>
								jQuery(document).ready(
									function() {
										jQuery("#topic-<?php echo $topic_position ?>-heading").on(
											"input",
											function(event) {
												program.topics[<?php echo $topic_position ?>].heading = event.target.value;
											}
										);
									}
								);
							</script>
							
							<div class="topic-audio-version-wrapper">
								<?php
									if ($value->has_audio) {
										$filename = "pirate-program-point-audio-" . $value->id;
										$full_location = path_join($upload_location, $filename);
										
										// Assume integrity
										$file = fopen($full_location, "r");
										
										$file_info = new finfo(FILEINFO_MIME);
										
										$file_buffer = fread($file, filesize($full_location));
										$media_type = explode(";", $file_info->buffer($file_buffer))[0];
										
										$data_uri_format_file = "data:" . $media_type . ";base64," . base64_encode($file_buffer);
										
										fclose($file);
										
										?>
										
											<script>
												program.topics[<?php echo $topic_position ?>].audio = "<?php echo $data_uri_format_file ?>";
											</script>
										
										<?php
									}
								?>
								
								<label
									for="topic-<?php echo $topic_position ?>-audio"
								>Audio verze:</label>
								<?php if ($value->has_audio) { ?>
								
									<audio
										id="topic-<?php echo $topic_position ?>-audio-player"
										controls
										src="<?php echo $data_uri_format_file ?>"
									>Tvůj prohlížeč nepodporuje přehrávání zvuku.</audio>
									
									<button id="topic-<?php echo $topic_position ?>-audio-delete">Odstranit</button>
									
									<script>
										jQuery(document).ready(
											function() {
												jQuery("#topic-<?php echo $topic_position ?>-audio-delete").on(
													"click",
													function() {
														program.topics[<?php echo $topic_position ?>].audio = null;
														
														jQuery("#topic-<?php echo $topic_position ?>-audio-player").remove();
														jQuery("topic-<?php echo $topic_position ?>-audio").val(null);
														event.target.remove();
													}
												);
											}
										);
									</script>
								
								<?php } ?>
								
								<input
									type="file"
									id="topic-<?php echo $topic_position ?>-audio"
									name="topic-<?php echo $topic_position ?>-audio"
									accept="audio/*"
								>
							</div>
							
							<script type="text/javascript">
								jQuery(document).ready(
									function() {
										jQuery("#topic-<?php echo $topic_position ?>-audio").on(
											"input",
											async function(event) {
												program.topics[<?php echo $topic_position ?>].audio = await getBase64(event.target.files[0]);
											}
										);
									}
								);
							</script>
							
							<label for="topic-<?php echo $topic_position ?>-description">Popis témata</label>
							
							<textarea
								id="topic-<?php echo $topic_position ?>-description"
								name="topic-<?php echo $topic_position ?>-description"
								rows="5"
								autocomplete="off"
							>
								<?php echo $value->description ?>
							</textarea>
							
							<script>
								jQuery(document).ready(
									function() {
										wp.editor.initialize(
											"topic-<?php echo $topic_position ?>-description",
											{
												tinymce: {
													init_instance_callback: function(editor) {
														editor.on(
															"change",
															function(event) {
																event.stopPropagation();
																program.topics[<?php echo $topic_position ?>].description = editor.getContent();
															}
														);
														
														editor.on(
															"input",
															function(event) {
																program.topics[<?php echo $topic_position ?>].description = editor.getContent();
															}
														);
													}
												}
											}
										);
									}
								);
							</script>
							
							<hr>
							
							<h2>Body témata</h2>
							
							<section class="point-wrapper">
								<section id="topic-<?php echo $topic_position ?>-points">
									<?php
										$point_position = 0;
										
										foreach ($value->points as $point) {
											?>
												
												<section id="topic-<?php echo $topic_position ?>-point-<?php echo $point_position ?>" class="point">
													<textarea
														id="topic-<?php echo $topic_position ?>-point-<?php echo $point_position ?>-content"
														name="topic-<?php echo $topic_position ?>-point-<?php echo $point_position ?>-content"
														rows="5"
														autocomplete="off"
													>
														<?php echo $point->text ?>
													</textarea>
													
													<button
														class="delete-button"
														id="topic-<?php echo $topic_position ?>-point-<?php echo $point_position ?>-delete"
													>Smazat bod</button>
													
													<script>
														jQuery(document).ready(
															function() {
																jQuery("#topic-<?php echo $topic_position ?>-point-<?php echo $point_position ?>-delete").on(
																	"click",
																	function() {
																		(
																			program.topics[<?php echo $topic_position ?>]
																			.points
																			.splice(<?php echo $point_position ?>, 1)
																		);
																		
																		jQuery("#topic-<?php echo $topic_position ?>-point-<?php echo $point_position ?>").remove();
																		jQuery("#save").click();
																	}
																);
																
																wp.editor.initialize(
																	"topic-<?php echo $topic_position ?>-point-<?php echo $point_position ?>-content",
																	{
																		tinymce: {
																			init_instance_callback: function(editor) {
																				// Work around TinyMCE's pasting bug(?)
																				editor.on(
																					"change",
																					function(event) {
																						event.stopPropagation();
																						
																						program.topics[<?php echo $topic_position ?>]
																						.points[<?php echo $point_position ?>]
																						.text = editor.getContent();
																					}
																				);
																				
																				editor.on(
																					"input",
																					function(event) {
																						program.topics[<?php echo $topic_position ?>]
																						.points[<?php echo $point_position ?>]
																						.text = editor.getContent();
																					}
																				);
																			}
																		}
																	}
																);
															}
														);
													</script>
												</section>
												
											<?php
											
											$point_position++;
										}
									?>
								</section>
								
								<button
									class="add-button"
									id="topic-<?php echo $topic_position ?>-add-point"
								>Přidat bod</button>
								
								<script>
									jQuery(document).ready(
										function() {
											jQuery("#topic-<?php echo $topic_position ?>-add-point").on(
												"click",
												function() {
													program.topics[<?php echo $topic_position ?>].points.push({text: "", id: ""});
													
													const pointNumber = program.topics[<?php echo $topic_position ?>].points.length - 1;
													
													jQuery(`#topic-<?php echo $topic_position ?>-points`).append(
														jQuery(`
															<section id="topic-<?php echo $topic_position ?>-point-${pointNumber}" class="point">
																<textarea
																	id="topic-<?php echo $topic_position ?>-point-${pointNumber}-content"
																	name="topic-<?php echo $topic_position ?>-point-${pointNumber}-content"
																	rows="5"
																></textarea>
																
																<button
																	class="delete-button"
																	id="topic-<?php echo $topic_position ?>-point-${pointNumber}-delete"
																>Smazat bod</button>
																
																\<script\>
																	jQuery("#topic-<?php echo $topic_position ?>-point-${pointNumber}-delete").on(
																		"click",
																		function() {
																			jQuery("#topic-<?php echo $topic_position ?>-point-${pointNumber}").remove();
																			
																			(
																				program.topics[<?php echo $topic_position ?>]
																				.points
																				.splice(${pointNumber}, 1)
																			);
																			
																			jQuery("#save").click();
																		}
																	);
																	
																	wp.editor.initialize(
																		"topic-<?php echo $topic_position ?>-point-${pointNumber}-content",
																		{
																			tinymce: {
																				init_instance_callback: function(editor) {
																					// Work around TinyMCE's pasting bug(?)
																					editor.on(
																						"change",
																						function(event) {
																							event.stopPropagation();
																							
																							program.topics[<?php echo $topic_position ?>]
																							.points[${pointNumber}]
																							.text = editor.getContent();
																						}
																					);
																					
																					editor.on(
																						"input",
																						function(event) {
																							program.topics[<?php echo $topic_position ?>]
																							.points[${pointNumber}]
																							.text = editor.getContent();
																						}
																					);
																				}
																			}
																		}
																	);
																\</script\>
															</section>
														`)
													);
												}
											);
										}
									);
								</script>
							</section>
							
							<button
								class="delete-button"
								id="topic-<?php echo $topic_position ?>-delete"
							>Smazat téma</button>
							
							<script>
								jQuery("#topic-<?php echo $topic_position ?>-delete").on(
									"click",
									function() {
										jQuery("#topic-<?php echo $topic_position ?>").remove();
										
										program.topics.splice(<?php echo $topic_position ?>, 1);
										
										jQuery("#save").click();
									}
								);
							</script>
						</section>
					<?php
					
					$topic_position++;
				}
			
			?>
		</section>
		
		<button
			class="add-button"
			id="topic-add"
		>Přidat téma</button>
		
		<br>
	<?php
}

function pirate_party_program_heading() {
	$heading = get_option("pirate_program_plugin_options")["heading"];
	
	?>
		
		<input
			type="text"
			id="pirate_program_plugin_options[heading]"
			name="pirate_program_plugin_options[heading]"
			value="<?php echo htmlspecialchars($heading) ?>"
			class="pirate-input"
			autocomplete="off"
		>
		
	<?php
}


function pirate_party_program_audio_version() {
	$has_audio = get_option("pirate_program_plugin_options")["has_audio"];
	$upload_location = wp_upload_dir()["basedir"];
	
	?>
		<div class="bottom-upload-wrapper">
	<?php
	
	if ($has_audio) {
		$filename = "pirate-program-full-audio";
		$full_location = path_join($upload_location, $filename);
		
		// Assume integrity
		$file = fopen($full_location, "r");
		
		$file_info = new finfo(FILEINFO_MIME);
		
		$file_buffer = fread($file, filesize($full_location));
		$media_type = explode(";", $file_info->buffer($file_buffer))[0];
		
		$data_uri_format_file = "data:" . $media_type . ";base64," . base64_encode($file_buffer);
	?>
	
		<script>
			program.audio_version = "<?php echo $data_uri_format_file ?>";
		</script>
		
		<audio
			id="program-audio-player"
			controls
			src="<?php echo $data_uri_format_file ?>"
		>Tvůj prohlížeč nepodporuje přehrávání zvuku.</audio>
		
		<button id="program-audio-delete">Odstranit</button>
		
		<script>
			jQuery(document).ready(
				function() {
					jQuery("#program-audio-delete").on(
						"click",
						function() {
							program.audio_version = null;
							
							jQuery("#program-audio-player").remove();
							jQuery("#pirate_program_plugin_options\\[audio_version\\]").val(null);
							event.target.remove();
						}
					);
				}
			);
		</script>
	
	<?php } ?>
	
	<input
		type="file"
		id="pirate_program_plugin_options[audio_version]"
		name="pirate_program_plugin_options[audio_version]"
		accept="audio/*"
	>
	</div>
	
	<script>
		jQuery("#pirate_program_plugin_options\\[audio_version\\]").on(
			"change",
			async function(event) {
				if (event.target.files.length === 0) {
					program.audio_version = null;
					return;
				}
				
				program.audio_version = await getBase64(event.target.files[0]);
			}
		);
	</script>
	
	<?php
}

function pirate_party_program_document_version() {
	$has_document = get_option("pirate_program_plugin_options")["has_document"];
	$upload_location = wp_upload_dir()["basedir"];
	
	?>
		<div class="bottom-upload-wrapper">
	<?php
	
	if ($has_document) {
		$filename = "pirate-program-document-version";
		$full_location = path_join($upload_location, $filename);
		
		// Assume integrity
		$file = fopen($full_location, "r");
		
		$file_info = new finfo(FILEINFO_MIME);
		
		$file_buffer = fread($file, filesize($full_location));
		$media_type = explode(";", $file_info->buffer($file_buffer))[0];
		
		$data_uri_format_file = "data:" . $media_type . ";base64," . base64_encode($file_buffer);
	?>
	
		<script>
			program.document_version = "<?php echo $data_uri_format_file ?>";
		</script>
		
		<button id="program-document-delete">Odstranit</button>
		
		<script>
			jQuery(document).ready(
				function() {
					jQuery("#program-document-delete").on(
						"click",
						function() {
							program.document_version = null;
							
							jQuery("#pirate_program_plugin_options\\[document_version\\]").val(null);
							event.target.remove();
						}
					);
				}
			);
		</script>
	
	<?php } ?>
	
	<input
		type="file"
		id="pirate_program_plugin_options[document_version]"
		name="pirate_program_plugin_options[document_version]"
	>
	</div>
	
	<script>
		jQuery("#pirate_program_plugin_options\\[document_version\\]").on(
			"change",
			async function(event) {
				if (event.target.files.length === 0) {
					program.document_version = null;
					return;
				}
				
				program.document_version = await getBase64(event.target.files[0]);
			}
		);
	</script>
	
	<?php
}

function pirate_party_program_link_newsletter() {
	$link_newsletter = get_option("pirate_program_plugin_options")["link_newsletter"];
	
	?>
	
		<input
			type="url"
			id="pirate_program_plugin_options[link_newsletter]"
			name="pirate_program_plugin_options[link_newsletter]"
			value="<?php echo htmlspecialchars($link_newsletter) ?>"
			class="pirate-input"
			autocomplete="off"
		>
	
	<?php
}

function pirate_party_program_link_onboarding() {
	$link_onboarding = get_option("pirate_program_plugin_options")["link_onboarding"];
	
	?>
	
		<input
			type="url"
			id="pirate_program_plugin_options[link_onboarding]"
			name="pirate_program_plugin_options[link_onboarding]"
			value="<?php echo htmlspecialchars($link_onboarding) ?>"
			class="pirate-input"
			autocomplete="off"
		>
	
	<?php
}

function pirate_party_program_link_donations() {
	$link_donations = get_option("pirate_program_plugin_options")["link_donations"];
	
	?>
	
		<input
			type="url"
			id="pirate_program_plugin_options[link_donations]"
			name="pirate_program_plugin_options[link_donations]"
			value="<?php echo htmlspecialchars($link_donations) ?>"
			class="pirate-input"
			autocomplete="off"
		>
	
	<?php
}


function pirate_party_program_post_id() {
	$post_id = get_option("pirate_program_plugin_options")["post_id"];
	
	?>
		
		<input
			type="text"
			id="pirate_program_plugin_options[post_id]"
			name="pirate_program_plugin_options[post_id]"
			value="<?php echo htmlspecialchars($post_id) ?>"
			class="pirate-input"
			autocomplete="off"
		>
		
	<?php
}

// Setting registration
function pirate_program_register_settings() {
	register_setting(
		"pirate_program_plugin_options",
		"pirate_program_plugin_options",
		"pirate_program_plugin_options_validate"
	);
	add_settings_section(
		"content",
		"Obsah",
		"pirate_program_section_text",
		"pirate_party_program"
	);
	add_settings_section(
		"dev",
		"Pro vývojáře, není nutno měnit",
		"pirate_program_dev",
		"pirate_party_program"
	);

	add_settings_field(
		"pirate_party_program_heading",
		"Nadpis",
		"pirate_party_program_heading",
		"pirate_party_program",
		"content"
	);
	add_settings_field(
		"pirate_party_program_content",
		"Témata",
		"pirate_party_program_content",
		"pirate_party_program",
		"content"
	);
	
	add_settings_field(
		"pirate_party_program_audio_version",
		"Audio verze",
		"pirate_party_program_audio_version",
		"pirate_party_program",
		"content"
	);
	add_settings_field(
		"pirate_party_program_document_version",
		"Verze v dokumentu",
		"pirate_party_program_document_version",
		"pirate_party_program",
		"content"
	);
	
	add_settings_field(
		"pirate_party_program_link_newsletter",
		"Odkaz na newsletter",
		"pirate_party_program_link_newsletter",
		"pirate_party_program",
		"content"
	);
	add_settings_field(
		"pirate_party_program_link_onboarding",
		"Odkaz na nalodění",
		"pirate_party_program_link_onboarding",
		"pirate_party_program",
		"content"
	);
	add_settings_field(
		"pirate_party_program_link_donations",
		"Odkaz na dary",
		"pirate_party_program_link_donations",
		"pirate_party_program",
		"content"
	);
	
	add_settings_field(
		"pirate_party_program_post_id",
		"ID Příspěvku",
		"pirate_party_program_post_id",
		"pirate_party_program",
		"dev"
	);
}

// Settings page
function pirate_program_add_settings_page() {
	add_options_page(
		"Nastavení pirátského programu",
		"Nastavení pirátského programu",
		"manage_options",
		"pirate_party_program",
		"pirate_program_render_plugin_settings_page"
	);
}

// JS to be loaded
function pirate_program_load_scripts() {
	wp_enqueue_script("jquery");
	wp_enqueue_editor();
}

// Database thing hooks
function pirate_program_activate() {
	global $wpdb;
	
	$table_name = $wpdb->prefix . "pirate_program_entries";
	
	$wpdb->query(
		"CREATE TABLE IF NOT EXISTS $table_name (
			id CHAR(36) NOT NULL,
			timestamp INTEGER NOT NULL DEFAULT 0,
			identifier VARCHAR(39),
			point VARCHAR(128) NOT NULL,
			PRIMARY KEY (id)
		);"
	);
	
	// https://wordpress.stackexchange.com/q/324597
	$post_table_name = $wpdb->prefix . "posts";
	
    if (
		$wpdb->get_row(
			"SELECT post_name FROM $post_table_name
			WHERE post_name = \"program\""
		) === null
	) {
		$post_id = wp_insert_post(
			Array(
				"post_author" => 1,
				"post_title" => "Program",
				"post_status" => "publish",
				"post_type" => "page",
				"post_slug" => "program"
			)
		);
		
		$options = get_option("pirate_program_plugin_options");
		$options["post_id"] = $post_id;
		
		update_option(
			"pirate_program_plugin_options",
			$options
		);
	}
}

// Custom page
function pirate_program_page_template($page_template){
	global $wp;
	
	if (is_page("program")) {
		$page_template = dirname( __FILE__ ) . "/pirate-program/display.php";
	}
	
	return $page_template;
}

// Get API
require_once(dirname( __FILE__ ) . "/pirate-program/api.php");

// Use filters
add_filter("page_template", "pirate_program_page_template");

// Use hooks
add_action("admin_menu", "pirate_program_register_settings");
add_action("admin_menu", "pirate_program_add_settings_page");
add_action("admin_enqueue_scripts", "pirate_program_load_scripts");
add_action("rest_api_init", function() {
	register_rest_route(
		"pirate-program",
		"/votes",
		Array(
			methods => Array("GET", "POST", "DELETE"),
			callback => "pirate_program_api"
		)
	);
});
register_activation_hook( __FILE__, "pirate_program_activate");


?>
