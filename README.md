# 2022 Czech Pirate Party election program WP plugin

A Wordpress plugin made for districts which, for whatever reason, use Wordpress for their websites but still want an interactive program available. There are better solutions, but this will have to do for now.

## Installation

- Make sure there are **no posts with the `program` slug**. If there are, change the slug, otherwise the plugin will break.
- Install like any other plugin, into `ROOT/wp-content/plugins` or through the builtin installer.
- Set up topics and points in the Admin control panel -> Settings -> *Nastavení pirátského programu*.

The program will then be available on the `program` post.

## Instalace (česká verze)

- Ujisti se, že **neexistuje žádný příspěvek, který má slug `program`**. Pokud tomu tak je, změň ho, plugin se jinak rozbije.
- Nainstaluj jako každý jiný plugin, do `ROOT/wp-content/plugins` nebo přes zabudovaný nástroj pro instalaci.
- Nastav témata a body v Administrátoské sekci -> Nastavení -> *Nastavení pirátského programu*.

Program bude dostupný na příspěvku `program`.
